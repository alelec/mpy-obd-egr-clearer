import time
from elm327.obd import OBDInterface
import alfa_emissions


class DpfMonitor(alfa_emissions.Alfa159):

    def current_dpf_regen(self):
        codes = {"current": ('21D91', None),
                 "dist since": ('21D41', lambda x: x/100.0),
                 "clogged": ('21CD1', lambda x: 3*x/200),
                 "filter temp": ("21C91", lambda x: 0.02 * x - 41.46)}

        vals = {}

        for desc, detail in codes.items():
            code, conv = detail
            retval = self.obd.send_command(code).strip()
            retval = self.check_response(retval)

            retval = retval.split('\r')

            try:
                val = int(retval[-1][10:-2], 16)
                if conv:
                    val = conv(val)
            except (ValueError, IndexError):
                self.logfile.write("dpf: err %s: %s\n" % (code, (','.join(retval))))
                val = -1
            vals[desc] = (val, retval)

        # self.logfile.write("dpf: %s\n" % vals)
        return vals.get('current', [0])[0] > 0
